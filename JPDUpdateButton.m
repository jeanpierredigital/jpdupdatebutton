//
//  JPDUpdateButton.m
//  Tradrs
//
//  Created by Justin Jean-Pierre on 2013-10-03.
//  Copyright (c) 2013 Jean-Pierre Digital Inc. All rights reserved.
//

#import "JPDUpdateButton.h"

@interface JPDUpdateButton () {
    float serverVersion;
    float currentVersion;
    BOOL appendVersion;
}

@property (nonatomic, readwrite) float serverVersion;
@property (nonatomic, readwrite) float currentVersion;
@property (nonatomic, readwrite) BOOL appendVersion;

@end

@implementation JPDUpdateButton

@synthesize defaultButtonText, updateAvailableButtonText, noUpdateAvailableButtonText;
@synthesize controlState, controlEvent;
@synthesize downloadURL, updateURL;
@synthesize serverVersion, currentVersion;
@synthesize appendVersion;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder { // designated initializer
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setDefault:@"button title"
              versionURL:@"http://sample-domain.com/my-app/version.plist"
             downloadURL:@"http://sample-domain.com/my-app/"
         updateAvailable:@"update available"
       noUpdateAvailable:@"no update available"
       controlState:UIControlStateNormal
         controlEvent:UIControlEventTouchUpInside];

        currentVersion = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"] floatValue];
    
        [self addTarget:self action:@selector(checkForUpdates:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return self;
}

-(void)setDefault:(NSString*)defaultTitle
       versionURL:(NSString *)uURL
      downloadURL:(NSString *)dURL
  updateAvailable:(NSString *)updateAvailText
noUpdateAvailable:(NSString *)noUpdateAvailText
     controlState:(NSUInteger)cState
     controlEvent:(NSUInteger)cEvent{
    
    // make sure values are valid.
    defaultTitle ? [self setDefaultButtonText:defaultTitle] : [self setDefaultButtonText:@"set button title text"];
    uURL ? [self setUpdateURL:uURL] : [self setUpdateURL:@""];
    dURL ? [self setDownloadURL:dURL] : [self setDownloadURL:@""];
    updateAvailText ? [self setUpdateAvailableButtonText:updateAvailText] : [self setUpdateAvailableButtonText:@"set update available text"];
    noUpdateAvailText? [self setNoUpdateAvailableButtonText:noUpdateAvailText] : [self setNoUpdateAvailableButtonText:@"set update available text"];
    controlState ? [self setControlState:cState] : [self setControlState:UIControlStateNormal];
    controlEvent ? [self setControlEvent:cEvent] : [self setControlEvent:UIControlEventTouchUpInside];
    appendVersion = YES;
    [self showDefaultText];
}

#pragma mark - button states
-(void)showDefaultText { // return button to its default state
    [self setTitle:defaultButtonText forState:controlState];
}

#pragma mark - button activities

-(IBAction)checkForUpdates:(id)sender {
    // really should check for internet reachability first, but for now just assume that's already done.
//    serverVersion = [[self latestRemoteVersion] floatValue];    // check custom plist for version
    serverVersion = [self latestVersionFromDefaultPlist];       // check default plist for version
    
    if ( serverVersion && (serverVersion > currentVersion) ) { // value exists and is higher than current
        [self removeTarget:self action:@selector(checkForUpdates:) forControlEvents:controlEvent];
        [self addTarget:self action:@selector(openDownloadURLInBrowser) forControlEvents:controlEvent];
        [self setTitle:updateAvailableButtonText forState:controlState];
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithFloat:serverVersion] forKey:@"current_version"];
    } else { // no updated version available
        [self appVersion];
        if (appendVersion == YES) {
            [self setTitle:[noUpdateAvailableButtonText stringByAppendingString:[NSString stringWithFormat:@" (v. %.2f)", [self latestLocalVersion]]] forState:controlState];
            [self setTitle:[noUpdateAvailableButtonText stringByAppendingString:[NSString stringWithFormat:@" (v. %.2f)", [self latestLocalVersion]]] forState:UIControlStateDisabled];
        }
        [self setEnabled:NO];
        [self removeTarget:self action:@selector(checkForUpdates:) forControlEvents:controlEvent];
        [self performSelector:@selector(resetButtonAfterFailedUpdateCheck) withObject:nil afterDelay:2];
    }
}

-(void)resetButtonAfterFailedUpdateCheck {
    [self setEnabled:YES];
    [self showDefaultText];
    [self addTarget:self action:@selector(checkForUpdates:) forControlEvents:controlEvent];
}

-(void)appVersion {
    currentVersion = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"] floatValue];
}

#pragma mark - Remote stuff

-(void)openDownloadURLInBrowser {
    // first, reset the button state
    [self showDefaultText];
    // button should no longer open browser...
    [self removeTarget:self action:@selector(openDownloadURLInBrowser) forControlEvents:controlEvent];
    // ...it should check for updates
    [self addTarget:self action:@selector(checkForUpdates:) forControlEvents:controlEvent];
    [self appVersion];
    
    // open the browser
    NSURL *URL = [NSURL URLWithString:downloadURL];
    [[UIApplication sharedApplication] openURL:URL];
}

-(float)latestLocalVersion {
    return [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"] floatValue];
}

-(NSNumber *)latestRemoteVersion {
    // this method parses a custom plist
    // use this if you don't want to have JPDUpdateButton parse Xcode's auto-generated plist
    NSURL *URL = [NSURL URLWithString:updateURL];
    NSDictionary *responseDictionary = [[NSDictionary alloc] initWithContentsOfURL:URL];
    NSNumber *versionFromServer = [NSNumber numberWithFloat:[[responseDictionary objectForKey:@"version_number"] floatValue]];
    
    return versionFromServer;
}

-(float)latestVersionFromDefaultPlist {
    // this method parses the plist that is automatically generated by Xcode's AdHoc distribution
    
    // guess at where the plist is
    // (or just use version URL?)
    NSBundle *bundle = [NSBundle mainBundle];
    NSDictionary *info = [bundle infoDictionary];
    NSString *prodName = [info objectForKey:@"CFBundleDisplayName"];
    NSURL *URL = [NSURL URLWithString:[[[[self updateURL] stringByDeletingLastPathComponent] stringByAppendingPathComponent:prodName] stringByAppendingPathExtension:@"plist"]];
    
    // parse the plist to find the version number
    NSDictionary *responseDictionary = [[NSDictionary alloc] initWithContentsOfURL:URL];
    NSArray *itemsArray = [responseDictionary objectForKey:@"items"];
    NSDictionary *metadataDictionary = [itemsArray objectAtIndex:0];
    NSDictionary *metadataInnerDict = [metadataDictionary objectForKey:@"metadata"];
    float bundleVersion = [[metadataInnerDict objectForKey:@"bundle-version"] floatValue];

    return bundleVersion;
}

@end
