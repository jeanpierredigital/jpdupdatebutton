//
//  JPDUpdateButton.h
//  Tradrs
//
//  Created by Justin Jean-Pierre on 2013-10-03.
//  Copyright (c) 2013 Jean-Pierre Digital Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JPDUpdateButton : UIButton {
    NSString *updateURL;
    NSString *downloadURL;
    NSString *defaultButtonText;
    NSString *updateAvailableButtonText;
    NSString *noUpdateAvailableButtonText;
    NSUInteger controlState;
    NSUInteger controlEvent;
}

@property (nonatomic, strong) NSString *updateURL;
@property (nonatomic, strong) NSString *downloadURL;
@property (nonatomic, strong) NSString *defaultButtonText;
@property (nonatomic, strong) NSString *updateAvailableButtonText;
@property (nonatomic, strong) NSString *noUpdateAvailableButtonText;
@property (nonatomic, readwrite) NSUInteger controlState;
@property (nonatomic, readwrite) NSUInteger controlEvent;

-(void)setDefault:(NSString*)defaultTitle
       versionURL:(NSString *)uURL
      downloadURL:(NSString *)dURL
  updateAvailable:(NSString *)updateAvailText
noUpdateAvailable:(NSString *)noUpdateAvailText
     controlState:(NSUInteger)cState
     controlEvent:(NSUInteger)cEvent;

-(IBAction)checkForUpdates:(id)sender;

@end
